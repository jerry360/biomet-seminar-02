#ifndef DETECTED_H
#define DETECTED_H

#endif // DETECTED_H

#include <iostream>
#include <vector>

using namespace std;

struct corditnates{
    int start[2][2];
    int end[2][2];
};

class File {
  public:
    string pathToImage;
    string pathToArea;
    int numberOfDetects;
    vector<corditnates> listOfCordinates;

    File(string pathImage,string pathArea){
        this->pathToImage=pathImage;
        this->pathToArea=pathArea;
        this->numberOfDetects=0;
    }

    string toString(){
        string output="";
        output+=(this->pathToArea)+"  --->  "+to_string(this->numberOfDetects);
        for (std::vector<corditnates>::iterator ct = this->listOfCordinates.begin() ; ct != this->listOfCordinates.end(); ++ct){
            output+="x:"+to_string(ct->start[0][0])
                    +", y:"+to_string(ct->start[0][1])
                    +" / x:"+to_string(ct->end[0][0])
                    +", y:"+to_string(ct->end[0][1])+"\n";
        }
        return output;
    }
};

class Files{
  public:
    string pathToDirImage;
    string pathToDirArea;
    vector<File> listOfFiles;

    Files(string pathImage,string pathArea){
        this->pathToDirImage=pathImage;
        this->pathToDirArea=pathArea;

    }

    string toString(){
        string output="";
        for (std::vector<File>::iterator it = this->listOfFiles.begin() ; it != this->listOfFiles.end(); ++it){
            output+=it->toString();

        }
        return output;

    }
};
