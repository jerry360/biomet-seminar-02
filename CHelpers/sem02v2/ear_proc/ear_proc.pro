TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
SOURCES += \
        main.cpp
        
# OpenCV
INCLUDEPATH += /usr/local/include/opencv4
LIBS += $(shell pkg-config opencv --libs)        
OPENCV = `pkg-config opencv4 --cflags --libs`

HEADERS += \
    detected.h \
    extractor.h \
    randomGenImage.h
