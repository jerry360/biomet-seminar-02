#ifndef EXTRACTOR_H
#define EXTRACTOR_H


#include <opencv2/opencv.hpp>
#include <iostream>
#include <dirent.h>
#include <sys/types.h>
#include <detected.h>

using namespace std;
using namespace cv;

typedef int (&cordinates)[2][2] ;
unsigned char isFile =0x8;


void findWhite(File &mfile);


void extractArea(Files *storage){

    struct dirent *entry;
    DIR *dir = opendir(storage->pathToDirArea.c_str());

    if (dir == NULL) {
       cout<<"empty dir!"<<endl;
       return;
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == isFile && strstr( entry->d_name, ".png" ))
        {
            cout<<"found: "<<entry->d_name<<endl;
//            File *tmpf=new File(storage->pathToDirImage+"/"+(entry->d_name),storage->pathToDirImage+"/"+(entry->d_name));
//            findWhite(*tmpf);
//            storage->listOfFiles.push_back(*tmpf);

        }
    }
    closedir(dir);

    File *tmpf=new File(storage->pathToDirImage+"/0001.png",storage->pathToDirArea+"/0001.png");
    storage->listOfFiles.push_back(*tmpf);

}


void findWhite(File &mfile){

    bool first=true;
    struct corditnates tmpc;

    Mat img = imread(mfile.pathToArea, 0);
    for(int i=0; i<img.rows; i++){
        for(int j=0; j<img.cols; j++){
            int pixel=img.at<uchar>(i,j);
            if (pixel>200)
            {
                if(first){
                    first=false;
                    mfile.numberOfDetects++;
                    tmpc.start[0][0]=i;
                    tmpc.start[0][1]=j;

                }
                tmpc.end[0][0]=i;
                tmpc.end[0][1]=j;
            }else{
                if(first==false){
                    first=true;
                    mfile.listOfCordinates.push_back(tmpc);
                }
            }
        }
    }
}



#endif // EXTRACTOR_H
