#include "opencv2/objdetect.hpp"
#include <opencv2/highgui.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include <iostream>
using namespace std;
using namespace cv;
void detectAndDisplay( Mat frame );
void checkOpenCL();
CascadeClassifier ears_cascade;
int main( int argc, const char** argv )
{
    String ears_cascade_name = "/home/me/Downloads/AWEForSegmentation/data/cascade.xml";

    if( !ears_cascade.load( ears_cascade_name ) )
    {
        cout << "--(!)Error loading cascade\n";
        return -1;
    };

    return 0;
}

void detectAndDisplay( Mat frame )
{
    Mat frame_gray;
    cvtColor( frame, frame_gray, COLOR_BGR2GRAY );
    equalizeHist( frame_gray, frame_gray );

    std::vector<Rect> faces;
    ears_cascade.detectMultiScale( frame_gray, faces );
    for ( size_t i = 0; i < faces.size(); i++ )
    {
        Point center( faces[i].x + faces[i].width/2, faces[i].y + faces[i].height/2 );
        ellipse( frame, center, Size( faces[i].width/2, faces[i].height/2 ), 0, 0, 360, Scalar( 255, 0, 255 ), 4 );
        Mat faceROI = frame_gray( faces[i] );

    }
    //-- Show what you got
    imshow( "Capture - Face detection", frame );
}
