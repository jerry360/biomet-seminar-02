import cv2
import sys
import glob
import ntpath

numberOfAll=0

def bb_intersection_over_union(boxA, boxB):
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)

    return interArea / float(boxAArea + boxBArea - interArea)




def detectEar(imgPath, clasPath):
    ears=0.0
    imagePath = imgPath
    cascPath = clasPath
    imageName=ntpath.basename(imagePath)
    negImagePath=pathToTrueImageNegatives+imageName

    faceCascade = cv2.CascadeClassifier(cascPath)

    image = cv2.imread(imagePath)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    foundEars = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30)
    )

    negImg = cv2.imread(negImagePath, 0)
    ret, thresh = cv2.threshold(negImg, 127, 255, 0)
    contours, hierarchy = cv2.findContours(thresh, 1, 1)

    for (x1, y1, w1, h1) in foundEars:
        cv2.rectangle(image, (x1, y1), (x1 + w1, y1 + h1), (0, 255, 0), 3)

        for tmpCnt in contours:
            x2, y2, w2, h2 = cv2.boundingRect(tmpCnt)
            cv2.rectangle(image, (x2, y2), (x2 + w2, y2 + h2), (0, 0, 255), 3)
            ears=ears+(bb_intersection_over_union([x1, y1, x1 + w1, y1 + h1],[x2, y2, x2 + w2, y2 + h2]))

    global numberOfAll
    numberOfAll += 1
    ears=ears/len(contours)
    print(negImagePath + "  " + imagePath + " // There were %d real ears / Found %d ears / IoU of detection is: %f !"%(len(contours),len(foundEars),ears))
    # cv2.imshow('ears', image)
    # cv2.waitKey(0)
    return ears


pathToTrueImagePositives="/home/me/Downloads/AWEForSegmentation/test/*.png"
pathToTrueImageNegatives="/home/me/Downloads/AWEForSegmentation/testannot_rect/"


positives = glob.glob(pathToTrueImagePositives)
positiveMatches=0
for tmpImg in positives:
    positiveMatches+=detectEar(tmpImg,"/home/me/Pictures/picture_of_ears_train_data/data/cascade.xml")

print("global IoU is %f !"%(positiveMatches/numberOfAll))
cv2.destroyAllWindows()