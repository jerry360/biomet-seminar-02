import let as let
import numpy as np
import cv2
import glob
import ntpath

def genImg(times):
    for i in range(times+1):
        rgb = np.random.randint(255, size=(400,400,3),dtype=np.uint8)
        cv2.imwrite(pathToNegatives+str(i)+".png", rgb)


def descPosImg():
    positives = glob.glob(pathToPositives)
    f = open(pathToInfo+"info.txt", "a")
    for tmpImg in positives:
        im = cv2.imread(tmpImg)
        h, w, _ = im.shape
        f.write(tmpImg + " 1 0 0 %d %d\n" % (w,h))

def descNegImg():
    negatives = glob.glob(pathToNegatives)
    f = open(pathToInfo+"bg.txt", "a")
    for tmpImg in negatives:
        f.write(tmpImg+"\n")



def findAreas():
    f = open(pathToTrueInfo + "info.txt", "a")
    positives = glob.glob(pathToTruePositives)
    for tmpImg in positives:
        img = cv2.imread(tmpImg,0)
        ret, thresh = cv2.threshold(img, 127, 255, 0)
        contours, hierarchy = cv2.findContours(thresh, 1, 1)
        f.write(pathToTrueImagePositives+ntpath.basename(tmpImg) +" "+str(len(contours)))
        print(pathToTrueImagePositives + ntpath.basename(tmpImg) + " " + str(len(contours)))
        for tmpCnt in contours:
            x, y, w, h = cv2.boundingRect(tmpCnt)
            f.write(" %d %d %d %d" % (x+1, y+1 , w-2 ,h-2 ))
            print (" %d %d %d %d" % (x, y , w ,h ))
        f.write("\n")
        print("\n")


def createNeg():
    positives = glob.glob(pathToTruePositives)
    for tmpImg in positives:
        img = cv2.imread(tmpImg,0)
        ret, thresh = cv2.threshold(img, 127, 255, 0)
        contours, hierarchy = cv2.findContours(thresh, 1, 1)
        img2 = cv2.imread(pathToTrueImagePositives + ntpath.basename(tmpImg))
        for tmpCnt in contours:
            x, y, w, h = cv2.boundingRect(tmpCnt)
            edited = cv2.rectangle(img2, (x, y), (x+w, y+h), (0, 0, 0), -1)

        cv2.imwrite(pathToTrueNegatives + ntpath.basename(tmpImg), edited)


def descTrueNegImg():
    negatives = glob.glob(pathToTrueNegatives)
    f = open(pathToTrueInfo+"bg.txt", "a")
    for tmpImg in negatives:
        f.write(tmpImg+"\n")





pathToPositives="/home/me/Pictures/picture_of_ears_train_data/positive/*.png"
pathToNegatives="/home/me/Pictures/picture_of_ears_train_data/rnd_unfilterd/*.jpg"
pathToInfo="/home/me/Pictures/picture_of_ears_train_data/"

pathToTruePositives="/home/me/Downloads/AWEForSegmentation/trainannot_rect/*.png"
pathToTrueImagePositives="/home/me/Downloads/AWEForSegmentation/train/"
pathToTrueInfo="/home/me/Downloads/AWEForSegmentation/"
pathToTrueNegatives="/home/me/Downloads/AWEForSegmentation/negatives/*.png"

#genImg(1000)
#descPosImg()
#descNegImg()
#findAreas()
#createNeg()
#descTrueNegImg()